from django.db import models


class ServerType(models.Model):
    name = models.CharField(max_length=20, blank=False, null=False)
    description = models.CharField(max_length=255, default='', blank=True)

    def __str__(self):
        return self.name


class Node(models.Model):
    name = models.CharField(max_length=50)
    node_type = models.ForeignKey(ServerType, on_delete=models.CASCADE)
    x_pos: models.IntegerField()
    y_pos: models.IntegerField()
    z_pos: models.IntegerField()

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Connector(Node):
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Service(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Server(Node):
    SERVER_STATES = (
        ('P', 'Planned'),
        ('A', 'Active'),
        ('J', 'In jeopardy'),
        ('D', 'Down'),
        ('X', 'Deleted'),
    )
    ip4_address = models.CharField(max_length=20, blank=True)
    ip6_address = models.CharField(max_length=36, blank=True)
    mem = models.FloatField(default=0)
    cores = models.IntegerField(default=1)
    disk_space = models.FloatField(default=0)
    state = models.CharField(max_length=15, choices=SERVER_STATES, default='P', null=False)

    class Meta:
        verbose_name_plural = 'Servers'

    def __str__(self):
        return self.name


class ServerServices(models.Model):
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    description = models.CharField(max_length=255)
    value = models.FloatField()

    class Meta:
        verbose_name_plural = 'Server-services'
        verbose_name = 'Server-service'
