from django.contrib import admin

from .models import Service, Server, Connector, ServerServices, ServerType

# TODO update this to use inlines, filters and the more


@admin.register(ServerType)
class ServerTypeAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    fields = ('name', 'description')


@admin.register(Server)
class ServerAdmin(admin.ModelAdmin):
    fields = ('name', 'node_type', 'ip4_address', 'ip6_address', 'mem', 'cores', 'state')


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    pass


@admin.register(Connector)
class ConnectorAdmin(admin.ModelAdmin):
    pass


@admin.register(ServerServices)
class ServerServicesAdmin(admin.ModelAdmin):
    pass
