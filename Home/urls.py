"""
The urls for the Home app designate the one page application displaying the Server application
"""

from .views import HomepageView

from django.urls import path

urlpatterns = [
    path('', HomepageView.as_view(), name='home'),
]
