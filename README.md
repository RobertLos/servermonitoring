# Server Monitoring
The purpose of this application is to visualise the status of servers and the tools running on that.
For this we will not just show lists, but it should be completely graphical interactive using
threejs.

All information is processed in javascript and retrieved
using a json based rest api

## Hosting
The application is hosted locally and on stepup server. In future versions we will
retrieve information using snmp to be nagios compliant.

## Setting up for running locally
We use the way of working as described in 'Two scoops of Django'. This means that we replaced
the settings.py with a settings folder. Inside this settings folder we have a base.py and a
local.py which contains the database settings for running locally. Database secrets are
imported through the environment space. This makes it easier to connect to external databases
or databases running on your local machine.

To run the application, either set the 'DJANGO_SETTINGS_MODULE=serverMonitoring.settings.local' environment
variable or add that to the command to run.

Requirements are handled in the same way. Do a pip install -r requirements/local.txt to load everything.

### Summary to start
```
git clone [this repo]
# create environment vars for MYSQL_DB, MYSQL_USER, MYSQL_PWD, MYSQL_HOST, MYSQL_PORT, DJANGO_SETTINGS_MODULE
# create a virtualenv with 
python3 -m venv venv
you can edit the activate script to set the environment variables
venv/bin/activate
(venv) $ pip install -r requirements/local.txt
(venv) $ python manage.py migrate
(venv) $ python manage.py collectstatic
(venv) $ python manage.py runserver
```

## Legal and license
Copyright (c) 2020 Robert Los Commercial and Contract Services B.V.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
