from .base import *  # ignore W064
import os

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.getenv('MYSQL_DB'),
        'USER': os.getenv('MYSQL_USER'),
        'PASSWORD': os.getenv('MYSQL_PWD'),
        'HOST': os.getenv('MSQL_HOST', 'localhost'),
        'PORT': os.getenv('MYSQL_PORT', 3306)
    }
}

ALLOWED_HOSTS = ['127.0.0.1',
                 '192.168.*',
                 'localhost',
                 ]
